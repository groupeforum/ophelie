<?php
/*
Plugin Name: Ophelie
Description: Ophelie is a superwoman who will keep an eye on your WordPress.
Version: 1.4.0
Author: Florian Girardey
Author URI: https://twitter.com/GIRARDEYFlorian
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/


if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


/**
 * Remove accents for media file name.
 *
 * @since 1.0.0
 */
add_filter( 'sanitize_file_name', 'remove_accents' );


/**
 * Remove the generator meta in the header and in the RSS Feed
 *
 * @since 1.0.0
 */
add_filter( 'the_generator', '__return_null' );
remove_action( 'wp_head', 'wlwmanifest_link' );


/**
 * Disable XML-RPC
 *
 * @since 1.2.0
 */
add_filter( 'xmlrpc_enabled', '__return_false' );


/**
 * Remove WP Rocket cache when deactivated
 *
 * @since 1.3.0
 */
add_action( 'deactivate_wp-rocket/wp-rocket.php', 'rocket_clean_domain' );


/**
 * Enable auto updates even if WordPress is under VCS (ie: Git or SVN)
 *
 * @since 1.4.0
 */
add_filter( 'automatic_updates_is_vcs_checkout', '__return_false', 1 );


if ( ! function_exists( 'ophelie_add_webmaster_role' ) ) {

	/**
	 * Create a 'Webmaster' role, an Editor++
	 *
	 * @since 0.4.0
	 *
	 * @return void
	 */
	function ophelie_add_webmaster_role() {
		if ( ( ! get_role( 'webmaster' ) ) AND $editor = get_role( 'editor' ) ) {
			add_role( 'webmaster', 'Webmaster', $editor->capabilities );

			$webmaster = get_role( 'webmaster' );

			// Allow the Webmaster to manage menu and theme options
			$webmaster->add_cap( 'edit_theme_options' );
			
			// WooCommerce core capabilities
			$webmaster->add_cap( 'manage_woocommerce' );
			$webmaster->add_cap( 'view_woocommerce_reports' );

			// WooCommerce product capabilities
			$webmaster->add_cap( 'edit_product' );
			$webmaster->add_cap( 'read_product' );
			$webmaster->add_cap( 'delete_product' );
			$webmaster->add_cap( 'edit_products' );
			$webmaster->add_cap( 'edit_others_products' );
			$webmaster->add_cap( 'publish_products' );
			$webmaster->add_cap( 'read_private_products' );
			$webmaster->add_cap( 'delete_products' );
			$webmaster->add_cap( 'delete_private_products' );
			$webmaster->add_cap( 'delete_published_products' );
			$webmaster->add_cap( 'delete_others_products' );
			$webmaster->add_cap( 'edit_private_products' );
			$webmaster->add_cap( 'edit_published_products' );
			$webmaster->add_cap( 'manage_product_terms' );
			$webmaster->add_cap( 'edit_product_terms' );
			$webmaster->add_cap( 'delete_product_terms' );
			$webmaster->add_cap( 'assign_product_terms' );

			// WooCommerce shop order capabilities
			$webmaster->add_cap( 'edit_shop_order' );
			$webmaster->add_cap( 'read_shop_order' );
			$webmaster->add_cap( 'delete_shop_order' );
			$webmaster->add_cap( 'edit_shop_orders' );
			$webmaster->add_cap( 'edit_others_shop_orders' );
			$webmaster->add_cap( 'publish_shop_orders' );
			$webmaster->add_cap( 'read_private_shop_orders' );
			$webmaster->add_cap( 'delete_shop_orders' );
			$webmaster->add_cap( 'delete_private_shop_orders' );
			$webmaster->add_cap( 'delete_published_shop_orders' );
			$webmaster->add_cap( 'delete_others_shop_orders' );
			$webmaster->add_cap( 'edit_private_shop_orders' );
			$webmaster->add_cap( 'edit_published_shop_orders' );
			$webmaster->add_cap( 'manage_shop_order_terms' );
			$webmaster->add_cap( 'edit_shop_order_terms' );
			$webmaster->add_cap( 'delete_shop_order_terms' );
			$webmaster->add_cap( 'assign_shop_order_terms' );

			// WooCommerce shop coupon capabilities
			$webmaster->add_cap( 'edit_shop_coupon' );
			$webmaster->add_cap( 'read_shop_coupon' );
			$webmaster->add_cap( 'delete_shop_coupon' );
			$webmaster->add_cap( 'edit_shop_coupons' );
			$webmaster->add_cap( 'edit_others_shop_coupons' );
			$webmaster->add_cap( 'publish_shop_coupons' );
			$webmaster->add_cap( 'read_private_shop_coupons' );
			$webmaster->add_cap( 'delete_shop_coupons' );
			$webmaster->add_cap( 'delete_private_shop_coupons' );
			$webmaster->add_cap( 'delete_published_shop_coupons' );
			$webmaster->add_cap( 'delete_others_shop_coupons' );
			$webmaster->add_cap( 'edit_private_shop_coupons' );
			$webmaster->add_cap( 'edit_published_shop_coupons' );
			$webmaster->add_cap( 'manage_shop_coupon_terms' );
			$webmaster->add_cap( 'edit_shop_coupon_terms' );
			$webmaster->add_cap( 'delete_shop_coupon_terms' );
			$webmaster->add_cap( 'assign_shop_coupon_terms' );

			// WooCommerce shop webhook capabilities
			$webmaster->add_cap( 'edit_shop_webhook' );
			$webmaster->add_cap( 'read_shop_webhook' );
			$webmaster->add_cap( 'delete_shop_webhook' );
			$webmaster->add_cap( 'edit_shop_webhooks' );
			$webmaster->add_cap( 'edit_others_shop_webhooks' );
			$webmaster->add_cap( 'publish_shop_webhooks' );
			$webmaster->add_cap( 'read_private_shop_webhooks' );
			$webmaster->add_cap( 'delete_shop_webhooks' );
			$webmaster->add_cap( 'delete_private_shop_webhooks' );
			$webmaster->add_cap( 'delete_published_shop_webhooks' );
			$webmaster->add_cap( 'delete_others_shop_webhooks' );
			$webmaster->add_cap( 'edit_private_shop_webhooks' );
			$webmaster->add_cap( 'edit_published_shop_webhooks' );
			$webmaster->add_cap( 'manage_shop_webhook_terms' );
			$webmaster->add_cap( 'edit_shop_webhook_terms' );
			$webmaster->add_cap( 'delete_shop_webhook_terms' );
			$webmaster->add_cap( 'assign_shop_webhook_terms' );
		}
	}
}

add_action( 'init', 'ophelie_add_webmaster_role' );


/**
 * Bugfix for MULTISTE
 *
 * @see   https://core.trac.wordpress.org/ticket/21573
 *
 * @since 0.7.0
 */
if ( defined( 'MULTISITE' ) AND MULTISITE ) {
	if ( defined( 'SUBDOMAIN_INSTALL' ) AND SUBDOMAIN_INSTALL ) {
		if ( defined( 'NOBLOGREDIRECT' ) AND NOBLOGREDIRECT ) {
			remove_action( 'template_redirect', 'maybe_redirect_404' );
		}
	}
}


if ( ! function_exists( 'ophelie_phpmailer_init' ) ) {

	/**
	 * Configuration for PHPMailer
	 *
	 * @param PHPMailer $phpmailer
	 */
	function ophelie_phpmailer_init( $phpmailer ) {

		// Send mail through MailDev if we are in local or dev environment
		if ( in_array( APP_ENV, array( 'local', 'dev' ) ) ) {

			// Set PHPMailer in SMTP mode
			$phpmailer->isSMTP();

			// Disable SSL verification
			$phpmailer->SMTPOptions = array(
				'ssl' => array(
					'verify_peer'       => false,
					'verify_peer_name'  => false,
					'allow_self_signed' => true,
				),
			);

			// Use maildev as SMTP host
			$phpmailer->Host = 'maildev';
		}
	}
}

add_action( 'phpmailer_init', 'ophelie_phpmailer_init' );
